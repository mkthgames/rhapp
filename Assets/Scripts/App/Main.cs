﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using strange.extensions.context.impl;

namespace App
{
	public class Main : ContextView
	{
		[SerializeField] Canvas _ui = null;
        [SerializeField] App.Components.SoundPlayer _soundPlayer = null;

		private WindowPresenter _windows = null;
		private Data _data = null;
        private static Main _instance = null;

		public static Main Instance
		{
			get {
				return _instance;
			}
		}

        public App.Components.SoundPlayer SoundPlayer
        {
            get {
                return _soundPlayer;
            }
        }

		public Data Data
		{
			get {
				return _data;
			}
		}
        

        public Canvas UI
		{
			get {
				return _ui;
			}
		}

		public WindowPresenter Windows
		{
			get {
				return _windows;
			}
		}

		void Awake()
		{
            Random.InitState(128);

			_data = gameObject.AddComponent<Data>();
			_windows = gameObject.AddComponent<WindowPresenter>();

			this.context = new AppContext(this);
			_instance = this;
			context.Start();
		}

	}
}