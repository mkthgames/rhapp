﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Views;

namespace App
{
	public class WindowPresenter : MonoBehaviour 
	{
		private Dictionary<Type, string> _prefabs = new Dictionary<Type, string>();
		private List<UIView> _windows = new List<UIView>();

		void Awake()
		{
			Setup();
		}

		public void Close(UIView window)
		{
			if (_windows.Contains(window)) {
				window.OnClose();
				_windows.Remove(window);
				Destroy(window.gameObject);
			}
		}

		public void CloseAll()
		{
			foreach (UIView window in _windows) {
				window.OnClose();
				_windows.Remove(window);
				Destroy(window.gameObject);
			}
		}

		public void Show<T>()
		{
			if (_prefabs.ContainsKey(typeof(T))) {
				GameObject prefab = App.Main.Instance.Data.GetResourceSync<GameObject>(_prefabs[typeof(T)]);
				if (prefab != null) {
					GameObject windowInstance = GameObject.Instantiate(prefab);
					UIView windowView = windowInstance.GetComponent<UIView>();
					if (windowView != null) {
						windowInstance.transform.SetParent(App.Main.Instance.UI.transform, false);
						_windows.Add(windowView);
						windowView.OnShow();
					} else {
						if (windowInstance != null) {
							Destroy(windowInstance.gameObject);
						}
					}
				}
			}
		}
	
		private void Setup()
		{
			_prefabs[typeof(MainMenuView)] = "Views/MainMenuView";
			_prefabs[typeof(LevelsView)] = "Views/LevelsView";
			_prefabs[typeof(LevelView)] = "Views/LevelView";
			_prefabs[typeof(LevelCompleteView)] = "Views/LevelCompleteView";
            _prefabs[typeof(LoginView)] = "Views/LoginView";
            _prefabs[typeof(InstructionsView)] = "Views/InstructionsView";
		}
	}
}