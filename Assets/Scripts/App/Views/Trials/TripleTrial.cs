﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Models;
using App.Widgets;
using App.Views.Widgets;

namespace App.Trials
{
    public class TripleTrial : TrialView
    {
        [SerializeField] List<CardWidget> Cards = null;
        [SerializeField] List<MathSignWidget> Signs = null;

        private int _flip = 0;

        private List<CardEntry> _deck = new List<CardEntry>();
        private int _trialsCount = 0;
        private int _current = 0;
        private bool _setup = false;
        private CardWidget _previousCard = null;

        public override void Setup(LevelEntry entry)
        {
            _current = 0;

            CardSuit[] filter = null;
            Debug.LogFormat("triple type = {0}", entry.type);

            switch (entry.type) {
                case "sum3":
                    foreach (MathSignWidget sign in Signs) {
                        sign.State = MathSignWidget.MathSignState.Add;
                        filter = new CardSuit[] { CardSuit.Clubs, CardSuit.Diamonds, CardSuit.Hearts, CardSuit.Spades };
                    }
                break;
                    
                default:
                    Debug.LogErrorFormat("Unsupported entry type \"{0}\"", entry.type);
                break;
            }

            _trialsCount = entry.count;

            for (int i = 0; i < _trialsCount; ++i) {
                CardEntry card = App.Utils.GetDistinctCard(_deck, 1, 11, _trialsCount, filter);
                _deck.Add(card);
            }
            Debug.Log("Trial setup");
            Debug.LogFormat("_deck size = {0}", _deck.Count);
        }

        private CardEntry PeekDeck()
        {
            if (_current + 1 > _deck.Count - 1) {
                return null;
            } else {
                CardEntry entry = _deck[_current];
                ++_current;
                return entry;
            }
        }

        public override int Current()
        {
            return _current;
        }

        public override int MaxTrials()
        {
            return _deck.Count;
        }

        public override void Start()
        {
            base.Start();

            if (!_setup) {
                foreach (CardWidget card in Cards) {
                    CardEntry deckEntry = PeekDeck();
                    if (deckEntry != null) {
                        card.Apply(deckEntry);
                        card.ForceCardState = CardState.Back;
                        card.OnCardFlip += OnStartFlip;
                        card.Flip();
                    } else {
                        throw new UnityException("Can't get distinct card from deck");
                    }
                }
                _setup = true;
            } 
            if (_previousCard != null) {
                CardEntry deckEntry = PeekDeck();
                if (deckEntry != null) {
                    _previousCard.OnCardFlip += OnStartFlip;
                    _previousCard.Flip();
                    _previousCard.Apply(deckEntry);
                } else {
                    Debug.Log("here");
                }
            }
        }

        private void OnStartFlip(CardWidget card)
        {
            card.OnCardFlip -= OnStartFlip;
            _animating = false;
        }

        public override string GetAnswer()
        {
            int sum = 0;
            foreach (CardWidget card in Cards) {
                sum += card.Value;
            }
            return sum.ToString();
        }

        private void OnCardFlip(CardWidget card)
        {
            card.OnCardFlip -= OnCardFlip;
            InvokeTaskEnd();
        }

        public override void Pass()
        {
            _animating = true;
            Cards[_flip].Flip();
            Cards[_flip].OnCardFlip += OnCardFlip;
            _previousCard = Cards[_flip];

//            _current++;
            _flip++;

            if (_flip >= Cards.Count) {
                _flip = 0;
            }
            base.Pass();
        }
    }
}