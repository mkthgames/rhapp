﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Models;
using App.Widgets;

namespace App.Trials
{
    public class MissFromTenTrial : TrialView
    {
        [SerializeField] CardWidget Card = null;
        private List<CardEntry> _deck = new List<CardEntry>();
        private int _trialCount = 0;
        private int _current = 0;
        private int _card = 0;

        public override int MaxTrials()
        {
            return _trialCount;
        }

        public override int Current()
        {
            return _current;
        }

        public override string GetAnswer()
        {
            return (_card - _deck[_current].Value).ToString();
        }

        public override void Pass()
        {
            Card.Flip();
            _animating = true;
            Card.OnCardFlip += OnCardFlip;
            _current++;
            Debug.LogFormat("Complete {0} task", _current);
            base.Pass();
        }

        public override void Setup(App.Models.LevelEntry entry)
        {
            base.Setup(entry);
            _trialCount = entry.count;
            _card = entry.card;
            for (int i = 0; i < _trialCount; ++i) {
                CardEntry card = App.Utils.GetDistinctCard(_deck, 1, 11, _trialCount, new CardSuit[] { CardSuit.Clubs });
                _deck.Add(card);
            }
        }

        private void OnStartFlip(CardWidget card)
        {
            card.OnCardFlip -= OnStartFlip;
            _animating = false;
        }

        private void OnCardFlip(CardWidget card)
        {
            card.OnCardFlip -= OnCardFlip;
            InvokeTaskEnd();
        }

        public override void Start()
        {
            _animating = true;
            Card.ForceCardState = CardState.Back;
            Card.Apply(_deck[_current]);
            Card.Flip();
            Card.OnCardFlip += OnStartFlip;
            base.Start();
        }
    }
}