﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Models;

namespace App.Trials
{
	public class CountTrial : TrialView
	{
		[SerializeField] GameObject CardPrefab = null;
		[SerializeField] Transform CardContainer = null;

		private List<int> Scenario = new List<int>();
		private int _current = 0;

		private void ClearContainer()
		{
			for (int i = 0; i < CardContainer.childCount; ++i)
			{
				Destroy(CardContainer.GetChild(i).gameObject);
			}
		}

        public override int MaxTrials()
        {
            return Scenario.Count;
        }

		public override string GetAnswer()
		{
			return Scenario[_current].ToString();
		}

		public override void Setup(LevelEntry entry)
		{

			for (int i = 0; i < 10; ++i) {
				Scenario.Add(App.Utils.GetDistinctNumber(Scenario, 1, 11, 10));
			}

		}

		public override void Start()
		{
			base.Start();

			ClearContainer();
			for (int i = 0; i < Scenario[_current]; ++i) {
				GameObject card = GameObject.Instantiate(CardPrefab);
				card.transform.SetParent(CardContainer, false);
			}
		}

		public override void Pass()
		{
			base.Pass();
			_current++;
            InvokeTaskEnd();
		}

		public override int Current()
		{
			return _current;
		}
	}
}