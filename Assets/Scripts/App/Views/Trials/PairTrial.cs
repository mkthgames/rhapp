﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Models;
using App.Views.Widgets;
using App.Widgets;

namespace App.Trials
{
    public class PairTrial : TrialView
	{
		[SerializeField] CardWidget LeftCard = null;
		[SerializeField] CardWidget RightCard = null;

        [SerializeField] MathSignWidget Sign = null;

		private int _current = 0;
		private LevelEntry _entry = null;

        private List<CardEntry> _deck = new List<CardEntry>();
        private int _trialsCount = 0;
        private FlippingRule _flippingRule = FlippingRule.FlipLeft;

		public override int Current()
		{       
			return _current;
		}
            
		public override void Setup(LevelEntry entry)
        {
            _entry = entry;
            CardSuit[] filter = null;

            switch (_entry.type) {
                default:
                    throw new UnityException(string.Format("Unsupported trial type '{0}'", _entry.type));

                case "sum_full":
                    Sign.State = MathSignWidget.MathSignState.Add;
                    filter = new CardSuit[] { CardSuit.Hearts, CardSuit.Clubs, CardSuit.Diamonds, CardSuit.Spades };
                    if (entry.card == -1) {
                        _flippingRule = FlippingRule.FlipSequental;
                    } else {
                        _flippingRule = FlippingRule.FlipRight;
                    }
                    _trialsCount = entry.count;
                break;

                case "sub_full":
                    Sign.gameObject.SetActive(false);

                    filter = new CardSuit[] { CardSuit.Hearts, CardSuit.Clubs, CardSuit.Diamonds, CardSuit.Spades };
                    _flippingRule = FlippingRule.FlipSequental;
                    _trialsCount = 40;
                break;

                case "sum":
                    Sign.State = MathSignWidget.MathSignState.Add;
                    filter = new CardSuit[] { CardSuit.Hearts };
                    Debug.Log("starting 'sum' trial");
                    _trialsCount = entry.count;
                break;

                case "sub":
                    Sign.gameObject.SetActive(false);
                    filter = new CardSuit[] { CardSuit.Hearts };
                    _deck.Add(new CardEntry { Suit = CardSuit.Hearts, Value = 10 });
                    _trialsCount = entry.count - 1;
                    Debug.Log("starting 'sub' trial");
                break;

                case "div":
                    Debug.Log("starting 'div' trial");
                    Sign.State = MathSignWidget.MathSignState.Div;

                    if (entry.count == 40) {
                        filter = new CardSuit[] { CardSuit.Hearts, CardSuit.Clubs, CardSuit.Diamonds, CardSuit.Spades };
                        _flippingRule = FlippingRule.FlipSequental;
                    } else {
                        filter = new CardSuit[] { CardSuit.Hearts };
                        _flippingRule = FlippingRule.FlipRight;
                    }
                break;

                case "mul":
                    Sign.State = MathSignWidget.MathSignState.Mul;
                    Debug.Log("starting 'mul' trial");
                    _flippingRule = FlippingRule.FlipRight;
                    _trialsCount = entry.count;

                    if (entry.count == 10) {
                        filter = new CardSuit[] { CardSuit.Hearts };
                    } else {
                        filter = new CardSuit[] { CardSuit.Hearts, CardSuit.Clubs, CardSuit.Diamonds, CardSuit.Spades };
                        if (entry.card == -1) {
                            _flippingRule = FlippingRule.FlipSequental;
                        }
                    }
                break;
            }

            if (entry.sequental) {
                for (int i = 0; i < _trialsCount; ++i) {
                    _deck.Add(new CardEntry { Suit = CardSuit.Hearts, Value = i + 1 });
                }
            } else {
                switch (_entry.type) {
                    default:
                        {
                            for (int i = 0; i < _trialsCount; ++i) {
                                CardEntry card = App.Utils.GetDistinctCard(_deck, 1, 11, _trialsCount, filter);
                                _deck.Add(card);
                            }
                        }
                    break;

                    case "sub_full":
                    case "div":
                        {
                            CardEntry card = App.Utils.GetDistinctCard(_deck, 6, 11, _trialsCount, filter);
                            while (card != null) {
                                _deck.Add(card);
                                if (_deck.Count % 2 == 0) {
                                    card = App.Utils.GetDistinctCard(_deck, 6, 11, _trialsCount, filter);
                                } else {
                                    card = App.Utils.GetDistinctCard(_deck, 1, 6, _trialsCount, filter);
                                }
                            }

                            _trialsCount = _deck.Count - 1;
                        }
                    break;
                }

            }

            if (_entry.type == "sub") {
                Utils.RemoveCard(ref _deck, CardSuit.Hearts, _entry.card);
            }

            switch (_flippingRule) {
                default:
                    throw new UnityException("");

                case FlippingRule.FlipRight:
                    LeftCard.Value = entry.card;
                    LeftCard.ForceCardState = CardState.Back;
                    LeftCard.Flip();
                break;

                case FlippingRule.FlipLeft:
                    RightCard.Value = entry.card;
                    RightCard.ForceCardState = CardState.Back;
                    RightCard.Flip();
                break;

                case FlippingRule.FlipSequental:
                    SetupCardWidget(LeftCard, _deck[_current]);
                    LeftCard.ForceCardState = CardState.Back;
                    LeftCard.Flip();

                    _current++;

                    SetupCardWidget(RightCard, _deck[_current]);
                    RightCard.ForceCardState = CardState.Back;
                    RightCard.Flip();
                break;
            }
           
			
		}

        private void SetupCardWidget(CardWidget card, CardEntry value)
        {
            card.Suit = value.Suit;
            card.Value = value.Value;

        }

        public override int MaxTrials()
        {
            return _trialsCount;
        }

        private CardWidget PickSequentalCard()
        {
            if (_current % 2 == 0) {
                return LeftCard;
            } else {
                return RightCard;
            }
        }

		public override void Start()
		{
            switch (_flippingRule) {
                default:
                    throw new UnityException(string.Format("Unsupported flipping rule '{0}'", _flippingRule));

                case FlippingRule.FlipRight:
                    RightCard.Apply(_deck[_current]);
                    RightCard.ForceCardState = CardState.Back;
                    RightCard.OnCardFlip += OnStartingFlip;
                    RightCard.Flip();

                break;
                    
                case FlippingRule.FlipLeft:
                    LeftCard.Apply(_deck[_current]);
                    LeftCard.ForceCardState = CardState.Back;
                    LeftCard.OnCardFlip += OnStartingFlip;
                    LeftCard.Flip();
                break;
                
                case FlippingRule.FlipSequental:
                    CardWidget card = PickSequentalCard();
                    card.Apply(_deck[_current]);
                    card.ForceCardState = CardState.Back;
                    card.OnCardFlip += OnStartingFlip;
                    card.Flip();
                break;
            }

			base.Start();

		}
        private void OnStartingFlip(CardWidget card)
        {
            card.OnCardFlip -= OnStartingFlip;

            _animating = false;
        }
		
        public override string GetAnswer()
		{
			switch (_entry.type) {
				default:
					return "Unsupported trial type";
				case "sum":
                    return (_entry.card + _deck[_current].Value).ToString();
                case "sub_full":
                    return (Mathf.Abs(LeftCard.Value - RightCard.Value)).ToString();
                case "sub":
                    return (Mathf.Abs(_entry.card - _deck[_current].Value)).ToString();
                case "div":
                    return Mathf.FloorToInt(LeftCard.Value / RightCard.Value).ToString();
                case "sum_full":     
                    return (LeftCard.Value + RightCard.Value).ToString();
                case "mul":
                    return (LeftCard.Value * RightCard.Value).ToString();
			}
		}

        private void OnCardFlip(CardWidget card)
        {
            
            card.OnCardFlip -= OnCardFlip;
            InvokeTaskEnd();
        }
        public override void Pass()
		{
			base.Pass();
            _animating = true;
            switch (_flippingRule) {
                default:
                case FlippingRule.FlipRight:
                    RightCard.OnCardFlip += OnCardFlip;
                    RightCard.Flip();
                    _current++;
                break;

                case FlippingRule.FlipLeft:
                    LeftCard.OnCardFlip += OnCardFlip;
                    LeftCard.Flip();
                    _current++;
                break;

                case FlippingRule.FlipSequental:
                    _current++;
                    CardWidget card = PickSequentalCard();
                    card.OnCardFlip += OnCardFlip;
                    card.Flip();

                break;
            }

		}
	}
}