﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using App.Models;
using JsonFx.Json;
using DG.Tweening;

namespace App.Views
{
    public class LoginView : UIView
    {
        [SerializeField] Text FeedbackText = null;
        [SerializeField] Text Username = null;
        [SerializeField] InputField Password = null;
        [SerializeField] List<GameObject> Controls = null;
        [SerializeField] GameObject LoadingLogo = null;

        [Inject]
        public PlayerModel PlayerModel { get; set; }

        [Inject]
        public AppModel AppModel { get; set; }

        private List<IConnectionModel> _connections = new List<IConnectionModel>();

        private string _username = "";
        private string _pwd = "";

        [PostConstruct]
        public void Init()
        {
            _connections.Add(new IslandicConnectionModel());
            //_connections.Add(new EnglishConnectionModel());
           // PlayerPrefs.DeleteAll();
          //  PlayerModel.SavedUsername = "";
           // PlayerModel.SavedPassword = "";
            if (!string.IsNullOrEmpty(PlayerModel.SavedUsername) &&
                !string.IsNullOrEmpty(PlayerModel.SavedPassword)) {
                _pwd = PlayerModel.SavedPassword;
                _username = PlayerModel.SavedUsername;
                FeedbackText.gameObject.SetActive(false);
                StartCoroutine(TryLogin());
                HideControls();
            }
        }

        private void HideControls()
        {
            LoadingLogo.SetActive(true);
            foreach (var control in Controls) {
                control.SetActive(false);
            }
        }

        private void ShowControls()
        {
            LoadingLogo.SetActive(false);
            foreach (var control in Controls) {
                control.SetActive(true);
            }
        }

        public void OnLogin()
        {
            if (!string.IsNullOrEmpty(Username.text)) 
            {
                _username = Username.text;
                _pwd = Password.text;

                FeedbackText.gameObject.SetActive(false);
                HideControls();
                StartCoroutine(TryLogin());
            }
        }

        private IEnumerator TryLogin()
        {
            bool _connectedViaTempPwd = false;
            bool _connected = false;

            foreach (var model in _connections) {
                if (_connected) break;
                
                Debug.LogFormat("Connection attempt. Locale '{0}'", model.Locale);
                WWWForm form = new WWWForm();
                form.AddField("apikey", model.ApiKey);
                form.AddField("apisecret", model.ApiSecret);
                form.AddField("email", _username);
                WWW request = new WWW(model.URL, form);

                yield return request;

                Debug.LogFormat("Response = '{0}'", request.text);
                if (!string.IsNullOrEmpty(request.text)) {
                    ResponseType data = null;
                    try
                    {
                        data = JsonFx.Json.JsonReader.Deserialize<ResponseType>(request.text);
                    } catch (Exception e) {
                        Debug.LogFormat("Error = {0}", e.Message);
                    }
                    if (data != null && data.Success)
                    {
                        var validIds = AppModel.MemberMouseFieldId;
                        string userPwd = "";
                        foreach (ResponseCustomField field in data.response_data.custom_fields)
                        {
                            if (field.id == AppModel.PwdCustomFieldID)
                            {
                                userPwd = field.value;
                            }
                        }

                        foreach (int validID in validIds)
                        {
                             //Debug.Log("checking with user password: " + userPwd + " AppState.PwdCustomFieldID " + AppModel.PwdCustomFieldID + " " + _pwd);
                             //Debug.Log("membership level = " + data.response_data.membership_level + " / is valid? " +(data.response_data.membership_level == validID.ToString()) + " / membership status: " + data.response_data.status + " / is valid? " + (AppModel.ValidMembershipStatus(data.response_data.status)));
                            if (!string.IsNullOrEmpty(userPwd) && userPwd == _pwd && data.response_data.membership_level == validID.ToString() && AppModel.ValidMembershipStatus(data.response_data.status))
                            {
                                LoggedInSuccesfully(model);
                                _connected = true;
                            }
                        }
                        /*
                        if (data.response_data[AppModel.LoginID] == _pwd && data.response_data.status != "2") {
                            PlayerModel.UserLocale = model.Locale;
                            _connected = true;
                            Debug.LogFormat("Connected in '{0}' domain", model.Locale);
                        }
                        */
                    }
                }
                /*
                }
                if (_connected) 
                {
                    PlayerModel.SavedPassword = _pwd;
                    PlayerModel.SavedUsername = _username;
                    OnLoggedIn();

                    PlayerPrefs.SetString("user", PlayerModel.SavedUsername);
                    PlayerPrefs.SetString("pwd", PlayerModel.SavedPassword);
                } else {
                    FeedbackText.gameObject.SetActive(true);
                    FeedbackText.text = StringsModel.Instance.Get("wrong_login");
                    ShowControls();
                }
                */


                if (!_connected)
                {
                    Debug.Log("checking tempPwd");
                    if (!string.IsNullOrEmpty(AppModel.TempPasswordField) && AppModel.TempPasswordField == _pwd)
                     {
                        LoggedInSuccesfully(model);
                         _connectedViaTempPwd = true;
                         _connected = true;
                    }
                }
             }

            if (_connected )
            {
                PlayerModel.SavedPassword = _pwd;
                PlayerModel.SavedUsername = _username;
                Debug.Log("connected via temp pwd? " + _connectedViaTempPwd);
                if(!_connectedViaTempPwd)
                {
                    PlayerPrefs.SetString("user", PlayerModel.SavedUsername);
                    PlayerPrefs.SetString("pwd", PlayerModel.SavedPassword);

                }
                else
                {
                    PlayerPrefs.SetString("user", string.Empty);
                    PlayerPrefs.SetString("pwd", string.Empty);

                }

                OnLoggedIn();
            } else {
                FeedbackText.gameObject.SetActive(true);
                FeedbackText.text = StringsModel.Instance.Get("wrong_login");
                ShowControls();
            }
        }


        private void LoggedInSuccesfully(IConnectionModel model)
        {
          //  StringsModel.Instance.SetLocale(model.Locale);

            Debug.LogFormat("Connected in '{0}' domain", model.Locale);
        }

        private void OnLoggedIn()
        {
            PlayerModel.LevelsMode = LevelsMode.Main;
            App.Main.Instance.Windows.Show<LevelsView>();
            App.Main.Instance.Windows.Close(this);
        }

        public void OnBack()
        {
            App.Main.Instance.Windows.Show<MainMenuView>();
            App.Main.Instance.Windows.Close(this);
        }
    }
}