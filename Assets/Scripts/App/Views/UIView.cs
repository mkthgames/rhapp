﻿using UnityEngine;
using System.Collections;

using strange.extensions.mediation.impl;

namespace App.Views
{
	public class UIView : View
	{
		public virtual void OnShow()
		{
		}

		public virtual void OnClose()
		{
		}
	}
}