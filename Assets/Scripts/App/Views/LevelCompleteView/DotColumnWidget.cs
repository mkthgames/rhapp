﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace App.Views.Widgets
{
    public class DotColumnWidget : MonoBehaviour
    {
        [SerializeField] List<float> Distribution = new List<float>();
        [SerializeField] List<GameObject> Bars = new List<GameObject>();

        private int _grade = -1;

        public float Seconds
        {
            set{
                for (int i = Distribution.Count - 1; i > 0; --i) {
                    if (value >= Distribution[i]) {
                        SetBar(i);
                        return;
                    }
                }
                SetBar(0);
            }
        }

        public int Grade {
            get {
                return _grade;
            }
        }

        public void SetBar(int index)
        {
            _grade = index;
            for (int i = 0; i < Bars.Count; ++i)
            {
                Bars[i].SetActive(i <= index);
            }
        }
    }
}