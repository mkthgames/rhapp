﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using App.Views;
using App.Models;
using App.Signals;

namespace App.Views
{
	public class LevelCompleteView : UIView
	{
//		public static int[] DotColors = {
//			0xe34f5f,
//			0xff8f0b,
//			0xffcc00,
//			0x5fc3ff,
//			0x0d86cb
//		};

        [SerializeField] Text LevelCompleteCaption = null;
		[SerializeField] Transform GraphRoot = null;
		[SerializeField] GameObject WidePrefab = null;
		[SerializeField] GameObject NarrowPrefab = null;
		[SerializeField] Text TimerText = null;
		[SerializeField] GameObject NextButton = null;
        [SerializeField] AudioClip LevelCompleteSound = null;
        [SerializeField] App.Widgets.LevelGaugeWidget GaugeWidget = null;

		[Inject]
		public AppModel AppModel { get; set; }

		[Inject]
		public PlayerModel PlayerModel { get; set; }

		[Inject]
		public StartLevelSignal StartLevelSignal { get; set; }

		[PostConstruct]
		public void Init()
		{
            LevelCompleteCaption.text = string.Format(StringsModel.Instance.Get("level_complete"), PlayerModel.SelectedLevelIndex + 1);
			switch (PlayerModel.LevelsMode) {
				case LevelsMode.Base:
					NextButton.SetActive(AppModel.BaseLevelsCount > (PlayerModel.SelectedLevelIndex + 1));
				break;
					
				case LevelsMode.Main:
					NextButton.SetActive(AppModel.MainLevelsCount > (PlayerModel.SelectedLevelIndex + 1));
				break;
			}
			GameObject prefab = null;
			if (PlayerModel.TrialTasksTime.Count == 10) {
				prefab = WidePrefab;
			} else {
				prefab = NarrowPrefab;
			}

            int totalGrade = 0;
			for (int i = 0; i < PlayerModel.TrialTasksTime.Count; ++i) {
				GameObject column = GameObject.Instantiate(prefab);
				App.Views.Widgets.DotColumnWidget widget = column.GetComponent<App.Views.Widgets.DotColumnWidget>();
				if (widget != null) {
					widget.Seconds = PlayerModel.TrialTasksTime[i];
                    totalGrade += widget.Grade;
				}
				column.transform.SetParent(GraphRoot, false);
			}

            GaugeWidget.Value = Mathf.RoundToInt((float)totalGrade / PlayerModel.TrialTasksTime.Count);

            TimerText.text = string.Format(StringsModel.Instance.Get("level_complete_time"), 
                App.Utils.FormatTimeMS((int)PlayerModel.TrialTimer.Elapsed.TotalSeconds));
            App.Main.Instance.SoundPlayer.PlayAudioClip(LevelCompleteSound);
		}


		public void OnMainMenu()
		{
			App.Main.Instance.Windows.Close(this);
			App.Main.Instance.Windows.Show<MainMenuView>();
		}

		public void OnRetryLevel()
		{
			App.Main.Instance.Windows.Close(this);
			StartLevelSignal.Dispatch();
		}

		public void OnNextLevel()
		{
			LevelEntry nextLevel = null;
			switch (PlayerModel.LevelsMode) {
				case LevelsMode.Base:
					nextLevel = AppModel.GetBaseLevel(PlayerModel.SelectedLevelIndex + 1);
				break;

				case LevelsMode.Main:
					nextLevel = AppModel.GetMainLevel(PlayerModel.SelectedLevelIndex + 1);
				break;
			}
			if (nextLevel != null) {
				PlayerModel.SelectedLevelIndex++;
				PlayerModel.SelectedLevel = nextLevel;
				App.Main.Instance.Windows.Close(this);
				StartLevelSignal.Dispatch();
			}
		}
	}
}