﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace App.Views.Widgets
{
	public class LevelMessageWidget : MonoBehaviour 
	{
		[SerializeField] Text _MessageText = null;
		[SerializeField] Text _ButtonCaption = null;
		public event Action OnClick;

		public string MessageText
		{
			set
			{
				if (_MessageText != null) {
					_MessageText.text = value;
				}
			}
		}

		public string ButtonCaption
		{
			set{
				if (_ButtonCaption != null) {
					_ButtonCaption.text = value;
				}
			}
		}

		public void OnButtonClick()
		{
			if (OnClick != null) {
				OnClick();
			}
		}
	}
}