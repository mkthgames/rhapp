﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using DG.Tweening;

namespace App.Views.Widgets
{
	public class LevelFeedbackWidget : MonoBehaviour 
	{
		[SerializeField] Text _FeedbackText = null;
		[SerializeField] float Duration = 5.0f;

		public event Action OnFeedbackEnd;

		public string FeebackText
		{
			set {  
				CancelInvoke();
				
                _FeedbackText.color = Color.white;

				if (_FeedbackText != null) {
					_FeedbackText.gameObject.SetActive(true);
					_FeedbackText.text = value;
					Invoke("OnComplete", Duration);
				}
			}
		}

		private void OnComplete()
		{
			_FeedbackText.gameObject.SetActive(false);
			if (OnFeedbackEnd != null)
			{
				OnFeedbackEnd();
			}
		}
	}
}