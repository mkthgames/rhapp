﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using App.Views;
using App.Models;

namespace App.Views.Widgets
{
	public class LevelTimerWidget : UIView
	{
		[SerializeField] Text TimerText;

		[Inject]
		public PlayerModel PlayerModel { get; set; }
		private bool _timerCounting = false;

		public void StartTimer()
		{
			_timerCounting = true;
		}

		public void StopTimer()
		{
			_timerCounting = false;
		}

		void LateUpdate()
		{
			if (_timerCounting) {
                TimerText.text = App.Utils.FormatTimeMS((int)PlayerModel.TrialTimer.Elapsed.TotalSeconds);
			}
		}
	}
}