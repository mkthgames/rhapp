﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using DG.Tweening;

using App.Models;

namespace App.Widgets
{
	public class CardWidget : MonoBehaviour 
	{
		[SerializeField] float FlipSpeed = 0.125f;
		[SerializeField] GameObject Back = null;
		[SerializeField] GameObject RedFront = null;
		[SerializeField] GameObject BlackFront = null;
        [SerializeField] Transform FrontRect = null;
		[SerializeField] List<GameObject> Cards = new List<GameObject>();

		[SerializeField] Sprite Hearts = null;
		[SerializeField] Sprite Diamonds = null;
		[SerializeField] Sprite Spades = null;
		[SerializeField] Sprite Clubs = null;

		[SerializeField] Text TopNumber = null;
		[SerializeField] Text BottomNumber = null;

        [SerializeField] Color RedSuitColor = Color.red;
        [SerializeField] Color BlackSuitColor = Color.black;

        public event Action<CardWidget> OnCardFlip;

        public CardState State = CardState.Back;

        private CardSuit _suit = CardSuit.Hearts;
        private int _value = -1;

		void Awake()
		{
			ForceCardState = CardState.Back;
            BlackFront.SetActive(false);
		}

		public CardSuit Suit
		{
			set {
                _suit = value;
				switch (_suit) {
                    case CardSuit.Hearts:
                    case CardSuit.Diamonds:
                        BlackFront.SetActive(true);
                        RedFront.SetActive(false);
                        TopNumber.color = BottomNumber.color = RedSuitColor;
					break;

					case CardSuit.Spades:
					case CardSuit.Clubs:
						BlackFront.SetActive(true);
						RedFront.SetActive(false);
                        TopNumber.color = BottomNumber.color = BlackSuitColor;
					break;
				}
			}
		}

		private void SetupSuitIcons(Transform container, int value)
		{
		    Sprite toSet = null;
            switch (_suit)
		    {
		        case CardSuit.Clubs:
		            toSet = Clubs;
		        break;
                case CardSuit.Diamonds:
		            toSet = Diamonds;
                break;
                case CardSuit.Hearts:
		            toSet = Hearts;
                break;
		        case CardSuit.Spades:
		            toSet = Spades;
                break;
		    }
            for (int i = 0; i < container.childCount; ++i) {
                Image iconHolder = container.GetChild(i).gameObject.GetComponent<Image>();
                if (iconHolder != null)
                {
                    iconHolder.sprite = toSet;
                    iconHolder.SetNativeSize();
                }
            }
		}

		public int Value
		{
			set {
                _value = value;
                TopNumber.text = BottomNumber.text = value.ToString();
				for (int i = 0; i < Cards.Count; ++i) {
                    if (i == (value - 1)) {
                        SetupSuitIcons(Cards[i].gameObject.transform, value);
                        Cards[i].gameObject.SetActive(true);
                    } else {
                        Cards[i].gameObject.SetActive(false);
                    }
					
				}
			}

            get {
                return _value;
            }
		}

		public CardState CardState
		{
			set {
				SetState(value);
			}
			get {
				return State;
			}
		}

		public void Flip(float delay = .0f)
		{
			if (delay > 0.005f) {
				Invoke("DoFlip", delay);
			} else {
				DoFlip();
			}
		}

		private void DoFlip()
		{
			CardState = State == CardState.Back ? CardState.Flipped : CardState.Back;
		}

		public CardState ForceCardState
		{
			set {
				State = value;
				switch (value) {
                    case CardState.Back:
                        RedFront.gameObject.SetActive(false);
                        BlackFront.gameObject.SetActive(false);
                        Back.gameObject.SetActive(true);
                        Vector3 scale = FrontRect.localScale;
                        scale.x = 0.0f;
                        FrontRect.localScale = scale;
						Back.gameObject.transform.localScale = Vector3.one;
					break;
						
					case CardState.Flipped:
						Back.gameObject.SetActive(false);
                        if (_suit == CardSuit.Diamonds || _suit == CardSuit.Hearts)
                        {
						    RedFront.gameObject.SetActive(true);
						    RedFront.gameObject.transform.localScale = Vector3.one;
                        } else
                        {
                            BlackFront.gameObject.SetActive(true);
                            BlackFront.gameObject.transform.localScale = Vector3.one;
                        }
					break;
				}
			}
		}

		private void SetState(CardState newState)
		{
			switch (newState) {
                case CardState.Back:
                    
                    FrontRect.DOScaleX(0.0f, FlipSpeed).OnComplete(() => {
						Back.gameObject.SetActive(true);
                        if (_suit == CardSuit.Diamonds || _suit == CardSuit.Hearts)
                        {
                            RedFront.gameObject.SetActive(true);
                            RedFront.gameObject.transform.localScale = Vector3.one;
                        } else
                        {
                            BlackFront.gameObject.SetActive(true);
                            BlackFront.gameObject.transform.localScale = Vector3.one;
                        }
						Vector3 temp = Back.transform.localScale;
						temp.x = 0.0f;
						Back.transform.localScale = temp;
                        Back.transform.DOScaleX(1.0f, FlipSpeed).OnComplete(()=> {
                            if (OnCardFlip != null && OnCardFlip.GetInvocationList().Length > 0)
                            {
                                OnCardFlip(this);
                            }
                        });
					});
				break;

				case CardState.Flipped:
                    
                    Back.transform.DOScaleX(0.0f, FlipSpeed).OnComplete(() => {
						Back.gameObject.SetActive(false);
                        if (_suit == CardSuit.Diamonds || _suit == CardSuit.Hearts)
                        {
                            RedFront.gameObject.SetActive(true);
                            RedFront.gameObject.transform.localScale = Vector3.one;
                        } else
                        {
                            BlackFront.gameObject.SetActive(true);
                            BlackFront.gameObject.transform.localScale = Vector3.one;
                        }
                        Vector3 temp = FrontRect.localScale;
						temp.x = 0.0f;
                        FrontRect.localScale = temp;
                        FrontRect.DOScaleX(1.0f, FlipSpeed).OnComplete(()=>{
                            if (OnCardFlip != null && OnCardFlip.GetInvocationList().Length > 0)
                            {
                                OnCardFlip(this);
                            }
                        });
					});
				break;
			}
			State = newState;
		}

        public void Apply(CardEntry entry)
        {
            Suit = entry.Suit;
            Value = entry.Value;
        }
	}
}