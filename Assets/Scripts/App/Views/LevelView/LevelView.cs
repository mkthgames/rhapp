﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using App.Models;
using App.Views;
using App.Trials;
using App.Views.Widgets;
using App.Signals;
using App.Components;

namespace App.Views
{

	public class LevelView : UIView
	{
		[SerializeField] LevelTimerWidget TimerWidget = null;
		[SerializeField] LevelFeedbackWidget FeedbackWidget = null;
		[SerializeField] LevelMessageWidget MessageWidget = null;
		[SerializeField] SwipeHandler SwipeHandler = null;
		[SerializeField] GameObject ConfirmExit = null;
		[SerializeField] Transform TrialContainer = null;
        [SerializeField] Text LevelCaption = null;
        [SerializeField] Toggle SoundToggle = null;

		[Inject]
		public PlayerModel PlayerModel { get; set; }

		[Inject]
		public AppModel AppModel { get; set; }

		[Inject]
		public ITrialsModel TrialsModel { get; set; }

		private TrialView _trial = null;
		private GameObject _trialView = null;

		[PostConstruct]
		public void Init()
		{
			TrialContainer.gameObject.SetActive(false);
			FeedbackWidget.gameObject.SetActive(false);
			TimerWidget.gameObject.SetActive(false);
			SwipeHandler.OnSwipe += SwipeHandler_OnSwipe;
			SwipeHandler.Enabled = false;

            LevelCaption.text = string.Format(StringsModel.Instance.Get("level_caption"), PlayerModel.SelectedLevelIndex + 1, 
                PlayerModel.LevelsMode == LevelsMode.Main ? AppModel.MainLevelsCount : AppModel.BaseLevelsCount);

			LevelEntry levelEntry = PlayerModel.SelectedLevel;
			string prefabName = TrialsModel.GetTrialViewName(levelEntry.type);
			if (prefabName != null) {
				GameObject trialPrefab = App.Main.Instance.Data.GetResourceSync<GameObject>(prefabName);
				if (trialPrefab != null) {
					_trialView = GameObject.Instantiate(trialPrefab);

					if (_trialView != null) {
						_trialView.transform.SetParent(TrialContainer, false);
						_trial = _trialView.GetComponent<TrialView>();
						_trial.Setup(levelEntry);
                        _trial.OnTaskEnd += _trial_OnCardFlipped;
                        switch (PlayerModel.LevelsMode) {
                            case LevelsMode.Base:
                                MessageWidget.MessageText = StringsModel.Instance.Get(string.Format("base_{0}_description", PlayerModel.SelectedLevelIndex));
                            break;
                            case LevelsMode.Main:
                                MessageWidget.MessageText = StringsModel.Instance.Get(string.Format("main_{0}_description", PlayerModel.SelectedLevelIndex));
                            break;
                            default:
                                throw new UnityException(string.Format("Unsupported player's levels mode {0}", PlayerModel.LevelsMode));
                        }

						MessageWidget.OnClick += OnMessageButtonClick;
					}
				}
			}

			TimerWidget.StartTimer();
		}

        public void OnSoundButton()
        {
            Main.Instance.SoundPlayer.SoundsOn = !SoundToggle.isOn;
            App.Main.Instance.SoundPlayer.PlayButtonClick();
        }

		void SwipeHandler_OnSwipe (SwipeDirection obj)
		{
            if (!_trial.IsBusy()) {
                FeedbackWidget.FeebackText = string.Format("{0}", _trial.GetAnswer());
                PlayerModel.TrialTasksTime.Add(_trial.Elapsed);
                App.Main.Instance.SoundPlayer.PlayCard(Utils.SpeedQualifier(_trial.Elapsed));
                if (_trial.Current() >= _trial.MaxTrials() - 1) {
                    PlayerModel.TrialTimer.Stop();
                    TimerWidget.StopTimer();
                    SwipeHandler.Enabled = false;
                    Invoke("EndLevel", 3.0f);
                } else { 
                    _trial.Pass();
                }
            }
		}

        void _trial_OnCardFlipped()
        {
            _trial.Start();
        }

		private void EndLevel()
		{
			App.Main.Instance.Windows.Show<LevelCompleteView>();
			App.Main.Instance.Windows.Close(this);
		}

		protected override void OnDestroy()
		{
			MessageWidget.OnClick -= OnMessageButtonClick;
		}

		private void OnMessageButtonClick()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
			MessageWidget.gameObject.SetActive(false);
			TrialContainer.gameObject.SetActive(true);
			TimerWidget.gameObject.SetActive(true);
			PlayerModel.TrialTimer.Start();
			SwipeHandler.Enabled = true;
			_trial.Start();
		}

		public void OnExit()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
			App.Main.Instance.Windows.Show<LevelsView>();
			App.Main.Instance.Windows.Close(this);
		}

		public void OnContinue()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
			ConfirmExit.SetActive(false);
			_trial.TimerEnabled = true;
			PlayerModel.TrialTimer.Start();
		}

		public void OnBack()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
            if (_trial != null) {
                _trial.TimerEnabled = false;
            }
			PlayerModel.TrialTimer.Stop();

			ConfirmExit.SetActive(true);
		}
	}
}