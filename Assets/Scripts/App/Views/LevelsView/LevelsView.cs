﻿using UnityEngine;
using System.Collections;

using strange.extensions.signal.impl;

using App.Models;
using App.Signals;
using App.Views;
using App.Views.Widgets;

namespace App.Views
{
	public class LevelsView : UIView
	{
		[SerializeField] Transform LevelContainer = null;	
		[SerializeField] GameObject LevelIconPrefab = null;

		[Inject]
		public PlayerModel PlayerModel { get; set; }

		[Inject]
		public AppModel AppModel { get; set; }

		[Inject]
		public StartLevelSignal StartLevelSignal { get; set; }

		private Signal<int> OnLevelSelected = new Signal<int>();

		[PostConstruct]
		public void Init()
		{
			OnLevelSelected.AddListener(LevelSelected);

			int count = PlayerModel.LevelsMode == LevelsMode.Main ? AppModel.MainLevelsCount : AppModel.BaseLevelsCount;
			for (int i = 0; i < count; ++i) {
				GameObject instance = GameObject.Instantiate(LevelIconPrefab);
				LevelBulletWidget widget = instance.GetComponent<LevelBulletWidget>();
				widget.OnLevelSelected = OnLevelSelected;
				widget.LevelNumber = i;
				instance.transform.SetParent(LevelContainer, false);
			}
		}

		private void LevelSelected(int level)
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
			switch (PlayerModel.LevelsMode) {
				case LevelsMode.Base:
					PlayerModel.SelectedLevel = AppModel.GetBaseLevel(level);
				break;

				case LevelsMode.Main:
					PlayerModel.SelectedLevel = AppModel.GetMainLevel(level);
				break;
			}

			PlayerModel.SelectedLevelIndex = level;
			StartLevelSignal.Dispatch();

			App.Main.Instance.Windows.Close(this);
		}

		public override void OnClose()
		{
			OnLevelSelected.RemoveListener(LevelSelected);
		}

		public void OnBack()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
			App.Main.Instance.Windows.Close(this);
			App.Main.Instance.Windows.Show<MainMenuView>();
		}
	}
}