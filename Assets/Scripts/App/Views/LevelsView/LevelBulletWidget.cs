﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using strange.extensions.signal.impl;

namespace App.Views.Widgets
{
	public class LevelBulletWidget : MonoBehaviour 
	{
		[SerializeField] Text LevelText = null;
		private int _levelNumber = -1;
		public Signal<int> OnLevelSelected = null;

		public void OnSelect()
		{
			if (OnLevelSelected != null) {
				OnLevelSelected.Dispatch(_levelNumber);
			}
		}

		public int LevelNumber
		{
			set {
				_levelNumber = value;
				if (LevelText != null) {
					LevelText.text = (_levelNumber + 1).ToString();
				}
			}
		}
	}
}