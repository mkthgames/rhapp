﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using App.Widgets;
using App.Components;
using App.Models;

namespace App.Views
{
    public class PairTutorial : MonoBehaviour 
    {
        [SerializeField] CardWidget LeftCard = null;
        [SerializeField] CardWidget RightCard = null;
        [SerializeField] SwipeHandler SwipeHandler = null;
        [SerializeField] Text MessageText = null;
        [SerializeField] GameObject AnimationDemo = null;

        private bool _firstSwap = true;

        void Awake()
        {
            SwipeHandler.OnSwipe += SwipeHandler_OnSwipe;
            SwipeHandler.Enabled = true;
            MessageText.text = StringsModel.Instance.Get("instructions_step_1");
        }

        void Start()
        {
            LeftCard.Apply(new CardEntry { Suit = CardSuit.Hearts, Value = 1 });
            RightCard.Apply(new CardEntry { Suit = CardSuit.Hearts, Value = 5 });
            LeftCard.CardState = CardState.Flipped;
            RightCard.CardState = CardState.Flipped;
        }

        void SwipeHandler_OnSwipe(SwipeDirection obj)
        {
            if (_firstSwap) 
            {
                _firstSwap = false;
                MessageText.text = StringsModel.Instance.Get("instructions_step_2");
                AnimationDemo.SetActive(false);
            }
            LeftCard.Flip();
            LeftCard.OnCardFlip += LeftCard_OnCardFlip;
        }

        void LeftCard_OnCardFlip (CardWidget obj)
        {
            LeftCard.OnCardFlip -= LeftCard_OnCardFlip;
            LeftCard.Flip();
        }

        public void OnBack()
        {
        }
    }
}