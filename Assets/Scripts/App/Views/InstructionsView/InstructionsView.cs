﻿using UnityEngine;
using System.Collections;

namespace App.Views
{
    public class InstructionsView : UIView
    {
        [SerializeField] GameObject GreetScreen = null;
        [SerializeField] GameObject TutorialScreen = null;

        [PostConstruct]
        void Init()
        {
            GreetScreen.SetActive(true);
            TutorialScreen.SetActive(false);
        }

        public void OnStart()
        {
            GreetScreen.SetActive(false);
            TutorialScreen.SetActive(true);
        }

        public void OnBack()
        {
            Main.Instance.Windows.Show<MainMenuView>();
            Main.Instance.Windows.Close(this);
        }
    }
}