﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace App.Widgets
{
    public class MathSignWidget : MonoBehaviour 
    {
        public enum MathSignState
        {
            Add = 0,
            Sub,
            Mul,
            Div
        }

        [SerializeField] List<GameObject> Signs = new List<GameObject>();

        private MathSignState _state = MathSignState.Add;

        void Awake()
        {
            State = MathSignState.Add;
        }

        public MathSignState State
        {
            get {
                return _state;
            }

            set {
                UpdateState(value);
            }
        }

        private void UpdateState(MathSignState newState)
        {
            Signs[(int)_state].SetActive(false);
            Signs[(int)newState].SetActive(true);
            _state = newState;
        }
    }
}