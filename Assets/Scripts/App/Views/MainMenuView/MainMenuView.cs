﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using App.Views;
using App.Models;
using App.Components;
using DG.Tweening;

namespace App.Views
{
	public class MainMenuView : UIView
	{
		[Inject]
		public PlayerModel PlayerModel { get; set; }

		public void OnInstructions()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
            App.Main.Instance.Windows.Show<InstructionsView>();
            App.Main.Instance.Windows.Close(this);
		}

		public void OnBaseLevels()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
			PlayerModel.LevelsMode = LevelsMode.Base;
			App.Main.Instance.Windows.Show<LevelsView>();
			App.Main.Instance.Windows.Close(this);
		}

		public void OnMainLevels()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
            App.Main.Instance.Windows.Show<LoginView>();
            App.Main.Instance.Windows.Close(this);
		}

		public void OnSettings()
		{
            App.Main.Instance.SoundPlayer.PlayButtonClick();
            PlayerModel.SwitchLocale();
            Localized[] widgets = GameObject.FindObjectsOfType<Localized>();
            foreach (Localized widget in widgets) 
            {
                widget.Localize();
            }
		}
	}
}