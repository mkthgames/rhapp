﻿using System;
using UnityEngine;
using System.Collections;

using App.Models;

namespace App.Trials
{
	public interface ITrial
	{
        event Action OnTaskEnd;
		string GetAnswer();
        int MaxTrials();
		void Setup(LevelEntry entry);
		void Pass();
		void Start();
		float Elapsed { get; }
		int Current();
        bool IsBusy();
	}
}