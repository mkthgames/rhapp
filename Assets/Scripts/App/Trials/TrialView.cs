﻿using System;
using UnityEngine;
using System.Collections;

using App.Models;

namespace App.Trials
{
	public class TrialView : MonoBehaviour, ITrial 
	{
		private bool _timeCount = false;
		private float _elapsed = 0.0f;
        protected bool _animating = false;
        public event Action OnTaskEnd;

        public void InvokeTaskEnd()
        {
            if (OnTaskEnd != null && OnTaskEnd.GetInvocationList().Length > 0)
            {
                OnTaskEnd();
            }
        }

		public bool TimerEnabled
		{
			set{
				_timeCount = value;
			}
		}

		public virtual int Current()
		{
			return -1;
		}

		public float Elapsed
		{
			get {
				return _elapsed;
			}
		}

		public virtual void Pass() {
			_timeCount = false;
		}

		public virtual void Start() {
			_elapsed = 0.0f;
			_timeCount = true;
		}

        public virtual int MaxTrials()
        {
            return -1;
        }

		public virtual void Setup(LevelEntry entry) {}

		public virtual void LateUpdate()
		{
			if (_timeCount) {
				_elapsed += Time.deltaTime;
			}
		}

        public virtual bool IsBusy()
        {
            return _animating;
        }

		public virtual string GetAnswer()
		{
			return "Override answer";
		}
	}
}