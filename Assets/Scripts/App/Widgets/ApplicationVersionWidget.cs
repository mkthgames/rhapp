﻿using UnityEngine;
using UnityEngine.UI;

public class ApplicationVersionWidget : MonoBehaviour {

    public Text VersionText = null;

	void Start()
    {
        if(VersionText != null)
        {
            VersionText.text = Application.version;
        }
    }
}
