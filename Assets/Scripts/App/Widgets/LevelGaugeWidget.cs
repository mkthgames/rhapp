﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace App.Widgets
{
    public class LevelGaugeWidget : MonoBehaviour 
    {
        [SerializeField] Transform GaugePin = null;
        [SerializeField] AudioClip NeedleSound = null;
        [SerializeField] AudioClip NeedleGong = null;

        private float[] _angles = 
        {
            -90.0f,
            -53.0f,
            -14.0f,
            25.0f,
            63.0f
        };

        public int Value {
            set {
                SetValue(value);
            }
        }

        private void SetValue(int value)
        {
            Debug.LogFormat("Setting needle to {0}", value);
            Vector3 endRotation = new Vector3(0, 0, _angles[value]);
            GaugePin.DORotate(endRotation, 1.0f).SetDelay(2.0f);
            if (Main.Instance.SoundPlayer.SoundsOn) {
                var audioSource = Main.Instance.SoundPlayer.AuxAudioSource;

                audioSource.volume = 1.0f;
                audioSource.pitch = 1.0f;

                audioSource.DOPitch(10.0f, 0.9f).SetDelay(2.0f).OnStart(() => {
                    audioSource.PlayOneShot(NeedleSound);
                });
                audioSource.DOFade(.0f, 0.1f).SetDelay(2.8f).OnComplete(() => {
                    if (value == 0) {
                        Main.Instance.SoundPlayer.PlayAudioClip(NeedleGong);
                    }
                });
            }
        }
    }
}