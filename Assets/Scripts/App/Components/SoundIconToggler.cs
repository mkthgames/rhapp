﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace App.Components
{
    public class SoundIconToggler : MonoBehaviour
    {
        [SerializeField] Toggle Toggle = null;
        void Start()
        {
            var SoundPlayer = Main.Instance.SoundPlayer;
            if (SoundPlayer && !SoundPlayer.SoundsOn) {
                Toggle.isOn = true;
            }
        }
    }
}