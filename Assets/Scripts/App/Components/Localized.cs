﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using App.Models;

namespace App.Components
{
    [RequireComponent(typeof(Text))]
    public class Localized : MonoBehaviour 
    {
        [SerializeField] string LocaleKey = string.Empty;
        [SerializeField] Text LocaleTarget = null;

        void Awake()
        {
            if (LocaleTarget == null) {
                LocaleTarget = this.GetComponent<Text>();

                if (LocaleTarget == null) {
                    throw new UnityException(string.Format("Can't access to Text component with key {0}", LocaleKey));
                }
            }
            Localize();
        }

        public void Localize()
        {
            if (StringsModel.Instance[LocaleKey] != null) {
                LocaleTarget.text = StringsModel.Instance[LocaleKey];
            }
        }
    }
}