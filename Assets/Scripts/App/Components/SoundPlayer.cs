﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Models;

namespace App.Components
{
    public class SoundPlayer : MonoBehaviour
    {
        [SerializeField] AudioSource AudioSource = null;
        [SerializeField] AudioSource FXAudioSource = null;

        [SerializeField] List<AudioClip> AudioClips = null;
        [SerializeField] AudioClip ButtonClickClip = null;

        private bool _soundsOn = true;

        public bool SoundsOn
        {
            get {
                return _soundsOn;
            }
            set {
                _soundsOn = value;
            }
        }

        public AudioSource AuxAudioSource {
            get {
                return FXAudioSource;
            }
        }

        public void PlayButtonClick()
        {
            if (SoundsOn) {
                AudioSource.PlayOneShot(ButtonClickClip);
            }
        }

        public void PlayAudioClip(AudioClip clip)
        {
            if (SoundsOn)
            {
                if (clip != null) {
                    AudioSource.PlayOneShot(clip);
                }
            }
        }

        public void PlayCard(int speedQualifier)
        {
            if (SoundsOn)
            {
                AudioSource.PlayOneShot(AudioClips[speedQualifier]);
            }
        }
    }
}