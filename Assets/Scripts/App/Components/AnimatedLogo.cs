﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using DG.Tweening;

namespace App.Components
{

	public class AnimatedLogo : MonoBehaviour {
		[SerializeField] GameObject LogoBack = null;
		[SerializeField] GameObject Logo = null;

		private static bool FirstRun = true;

		void Awake()
		{
			if (FirstRun) {
				FirstRun = false;
				RectTransform rt = Logo.GetComponent<RectTransform>();
                
				if (rt != null) {
	                rt.transform.localScale = Vector3.one * 1.5f;
                    rt.transform.DOScale(Vector3.zero, 0.3f).SetDelay(0.3f);
                    Image image = LogoBack.gameObject.GetComponent<Image>();
					DOTween.To(() => image.color.a, (x) => {
						Color t = image.color;
						t.a = x;
						image.color = t;
					}, 0.0f, 0.3f).SetDelay(0.75f).OnComplete(() => {
						LogoBack.gameObject.SetActive(false);
					});
				}
			} else {
				LogoBack.SetActive(false);
			}
		}
	}
}