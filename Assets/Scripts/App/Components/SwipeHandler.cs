﻿using System;
using UnityEngine;
using System.Collections;

namespace App.Components
{
	public enum SwipeDirection
	{
		Left = 0,
		Right,
	}

	public class SwipeHandler : MonoBehaviour 
	{
		public const float MAX_DURATION = 1.0f;
		public const float MAX_DISTANCE = 50.0f;
		public const float MIN_DISTANCE = 15.0f;
		public event Action<SwipeDirection> OnSwipe;

		private bool _enabled = false;

		private bool _gesturing = false;
		private Vector3 _startPos = default(Vector3);
		private float _elapsed = 0.0f;
		private bool _clickProof = false;

		public bool Enabled
		{
			set {
				_enabled = value;
			}

			get {
				return _enabled;
			}
		}

		void LateUpdate()
		{
			if (!_enabled)
				return;
			
			if (Input.GetMouseButtonDown(0)) {
				_clickProof = true;
				_startPos = Input.mousePosition;
			}

			if (_clickProof) {
				Vector3 current = Input.mousePosition - _startPos;
				if (current.magnitude > 5) {
					_clickProof = false;
					_elapsed = .0f;
					_gesturing = true;
				}
			}

			if (_gesturing) {
				_elapsed += Time.deltaTime;
				if (_elapsed > MAX_DURATION) {
					_gesturing = false;
				} else {
					Vector3 current = Input.mousePosition - _startPos;
					if (current.magnitude >= MAX_DISTANCE) {
						ProcessSwipe();
					}
				}
			}

			if (Input.GetMouseButtonUp(0)) {
				_clickProof = false;
				if (_gesturing)
				{
					Vector3 current = Input.mousePosition - _startPos;
					if (current.magnitude >= MIN_DISTANCE) {
						ProcessSwipe();
					}
				}
			}
		}

		private void ProcessSwipe()
		{
			_gesturing = false;
			if (OnSwipe != null) {
				OnSwipe(SwipeDirection.Right);
			}
		}
	}
}