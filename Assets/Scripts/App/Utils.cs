﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Models;
using App.Trials;

namespace App
{
	public static class Utils
	{
        public static void RemoveCard(ref List<CardEntry> deck, CardSuit suit, int value)
        {
            deck.RemoveAll((x)=>{
                return x.Suit == suit && x.Value == value;
            });
        }

        public static int SpeedQualifier(float time)
        {
            float[] milestones = new float[] {
                1.0f,
                2.0f,
                3.0f,
                4.0f,
                5.0f
            };

            for (int i = milestones.Length - 1; i >= 0; i--) {
                if (time >= milestones[i])
                    return i;
            }
            return 0;
        }

		public static int UnixTimestamp ()
		{
			return (int)(System.Math.Truncate ((System.DateTime.UtcNow.Subtract (new System.DateTime (1970, 1, 1))).TotalSeconds));
		}

		public static string FormatTimeMS(float time)
		{
			int mins = (int)(time / 60.0f);
			int secs = (int)(time - mins * 60);

			return string.Format("{0,2}:{1,2}", mins, secs > 9 ? secs.ToString() : "0" + secs);
		}

        public static CardEntry GetDistinctCard(List<CardEntry> deck, int @from, int to, int maxCapacity, CardSuit[] filter)
        {
            Dictionary<CardSuit, List<int>> cards = new Dictionary<CardSuit, List<int>>();
            Dictionary<CardSuit, List<int>> avail = new Dictionary<CardSuit, List<int>>();

            // broke deck into sorted dictinary
            foreach (CardEntry card in deck) {
                if (!cards.ContainsKey(card.Suit)) {
                    cards[card.Suit] = new List<int>();
                }
                cards[card.Suit].Add(card.Value);
            }


            // resolve avail cards by suit
            foreach (CardSuit suit in filter) {
                for (int i = @from; i < to; ++i) {
                    if (cards.ContainsKey(suit)) {
                        if (!cards[suit].Contains(i)) {
                            
                            if (!avail.ContainsKey(suit)) {
                                avail[suit] = new List<int>();
                            }
                            avail[suit].Add(i);
                        }
                    } else {
                        if (!avail.ContainsKey(suit)) {
                            avail[suit] = new List<int>();
                        }
                        avail[suit].Add(i);
                    }

                }
            }

            CardSuit[] keys = new CardSuit[avail.Keys.Count];
            avail.Keys.CopyTo(keys, 0);
            if (avail.Count == 0)
                return null;

            List<CardSuit> availSuits = new List<CardSuit>();
            foreach (var key in keys) {
                if (avail[key].Count > 0) {
                    availSuits.Add(key);
                }
            }
            if (availSuits.Count == 0) return null;

            CardSuit randomSuit = availSuits[Random.Range(0, availSuits.Count)];
            if (avail[randomSuit].Count > 0) {
                int randomValue = avail[randomSuit][Random.Range(0, avail[randomSuit].Count)];
                return new CardEntry() {
                    Suit = randomSuit,
                    Value = randomValue
                };
            } 
            return null;
        }

		public static int GetDistinctNumber(List<int> busy, int @from, int to, int maxCapacity)
		{
			if (busy.Count >= maxCapacity)
				return 0;
			List<int> free = new List<int>();
			for (int i = from; i < to; ++i) {
				if (!busy.Contains(i)) {
					free.Add(i);
				}
			}
			if (free.Count > 0) {
				return free[Random.Range(0, free.Count)];
			} else
				return 0;
		}

	}
}