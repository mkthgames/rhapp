﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace App
{
	public class Data : MonoBehaviour 
	{
		public string ReadTextFile(string fileName)
		{
			string actualName = Path.GetFileNameWithoutExtension(fileName);
			TextAsset text = GetResourceSync<TextAsset>(actualName);
			return text.text;
		}

		public T GetResourceSync<T>(string resourceName) where T: Object
		{
			Debug.LogFormat("GetResourceSync({0})", resourceName);
			return Resources.Load<T>(resourceName);
		}

        public void LoadJsonFromUrl(string urlpath, System.Action<string> OnLoad)
        {
            Debug.Log(urlpath);
            StartCoroutine(LoadUrlJson(urlpath, OnLoad));
        }

        public IEnumerator LoadUrlJson(string urlpath, System.Action<string> onLoad)
        {
            var timestamp = Timestamp();

            string url = urlpath + "?rnd=" + timestamp;
            Debug.LogFormat("Loading '{0}'", url);
            WWW www = new WWW(url);
            yield return www;
            onLoad(www.text);
        }


        public static long Timestamp()
        {
            var epochStart = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
            return (long)(System.DateTime.UtcNow - epochStart).TotalSeconds;
        }
    }
}