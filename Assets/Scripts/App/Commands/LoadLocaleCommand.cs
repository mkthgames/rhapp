﻿using UnityEngine;
using System.Collections;

using strange.extensions.command.impl;
using App.Models;

namespace App.Commands
{
    public class LoadLocaleCommand : Command
    {
        [Inject]
        public PlayerModel PlayerModel { get; set; }

        public override void Execute()
        {
            StringsModel.Instance.Init(PlayerModel.UserLocale);
        }
    }
}