﻿using UnityEngine;
using System.Collections;

using strange.extensions.command.impl;

using App;
using App.Views;
using App.Models;

namespace App.Commands
{
	public class StartAppCommand : Command
	{
		[Inject]
		public PlayerModel PlayerModel { get; set; }

		[Inject]
		public ITrialsModel trialsModel { get; set; }

		public override void Execute()
		{
			trialsModel.AddTrialViewName("sum", "Views/Trials/PairTrialView");
			trialsModel.AddTrialViewName("mul", "Views/Trials/PairTrialView");
			trialsModel.AddTrialViewName("div", "Views/Trials/PairTrialView");
			trialsModel.AddTrialViewName("sub", "Views/Trials/PairTrialView");
			trialsModel.AddTrialViewName("count", "Views/Trials/CountTrialView");
            trialsModel.AddTrialViewName("sum_full", "Views/Trials/PairTrialView");
            trialsModel.AddTrialViewName("sub_full", "Views/Trials/PairTrialView");
            trialsModel.AddTrialViewName("sum3", "Views/Trials/TripleTrialView");
            trialsModel.AddTrialViewName("miss", "Views/Trials/MissFromTenView");

			App.Main.Instance.Windows.Show<MainMenuView>();
		}
	}
}