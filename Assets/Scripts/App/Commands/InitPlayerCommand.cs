﻿using UnityEngine;
using System.Collections;

using strange.extensions.command.impl;
using App.Models;


namespace App.Commands
{
    public class InitPlayerCommand : Command
    {
        [Inject]
        public PlayerModel PlayerModel { get; set; }

        public override void Execute()
        {
            PlayerModel.SavedPassword = PlayerPrefs.GetString("pwd", string.Empty);
            PlayerModel.SavedUsername = PlayerPrefs.GetString("user", string.Empty);
            PlayerModel.UserLocale = PlayerPrefs.GetString("locale", LocaleStrings.LOCALE_IS);
        }
    }
}