﻿using System;
using UnityEngine;
using System.Collections;

using strange.extensions.command.impl;

using App.Models;

namespace App.Commands
{
	public class LoadConfigCommand : Command
	{
		[Inject]
		public AppModel AppModel { get; set; }

        private bool _useLocal = false;
        public static string urlConfig;

        public override void Execute()
        {
            if (_useLocal)
            {
                Debug.Log("using local config");
                var localData = "";
                localData = App.Main.Instance.Data.ReadTextFile("config");
                AppModel.InitLevels(localData);
            }
            else
            {
                Debug.Log("using online config");
                string url = "https://www.betranam.is/reiknumhradar2/libraries/config.txt";
                App.Main.Instance.Data.LoadJsonFromUrl(url, OnLoad);
            }
        }

        private void OnLoad(string data)
        {
            AppModel.InitLevels(data);
        }
    }
   
}