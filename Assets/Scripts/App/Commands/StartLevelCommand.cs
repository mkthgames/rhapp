﻿using UnityEngine;
using System.Collections;

using strange.extensions.command.impl;
using App.Models;
using App.Views;

namespace App.Commands
{
	public class StartLevelCommand : Command
	{
		[Inject]
		public PlayerModel PlayerModel { get; set; }

		public override void Execute()
		{
			PlayerModel.TrialTasksTime.Clear();
			PlayerModel.TrialTimer.Reset();

			App.Main.Instance.Windows.Show<LevelView>();
		}
	}
}