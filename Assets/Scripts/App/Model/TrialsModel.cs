﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using App.Trials;

namespace App.Models
{
	public interface ITrialsModel
	{
		string GetTrialViewName(string trialName);
		void AddTrialViewName(string trialName, string trialViewName);
	}

	public class TrialsModel: ITrialsModel
	{
		private Dictionary<string, string> _trialViews = new Dictionary<string, string>();

		public void AddTrialViewName(string trialName, string trialViewName)
		{
			if (!_trialViews.ContainsKey(trialName)) {
				_trialViews[trialName] = trialViewName;
			} else {
				Debug.LogErrorFormat("The trial handler for '{0}' already registered (containts={1}, new={2})", trialName, _trialViews[trialName], trialViewName);
			}
		}

		public string GetTrialViewName(string trialName)
		{
			if (_trialViews.ContainsKey(trialName)) {
				return _trialViews[trialName];
			}
			return null;
		}


	}
}