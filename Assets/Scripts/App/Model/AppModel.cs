﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using JsonFx.Json;

namespace App.Models
{
	public class AppModel
	{
		private LevelsModel Levels = null;
        public string tempPwd;

        public List<int> MemberMouseFieldId
        {
            get
            {
                return Levels.MMid;
            }
        }

        public string TempPasswordField
        {
            get
            {
                return Levels.tempPwd;
            }
        }

        public string PwdCustomFieldID
        {
            get
            {
                return Levels.MMPwdCustomFieldID;
            }
        }

        public bool ValidMembershipStatus(string membershipStatus)
        {
            return Levels.validStatus.Contains(membershipStatus);
        }



        public int BaseLevelsCount
		{
			get {
				if (Levels == null || Levels.baseLevels == null)
					return 0;
				return Levels.baseLevels.Count;
			}
		}

		public int MainLevelsCount
		{
			get {
				if (Levels == null || Levels.mainLevels == null)
					return 0;
				return Levels.mainLevels.Count;
			}
		}

		public LevelEntry GetBaseLevel(int index)
		{
			if (index < 0)
				return null;
			
			if (index >= 0 && index < Levels.baseLevels.Count) {
				return Levels.baseLevels[index];
			}
			return null;
		}

		public LevelEntry GetMainLevel(int index)
		{
			if (index < 0)
				return null;

			if (index >= 0 && index < Levels.mainLevels.Count) {
				return Levels.mainLevels[index];
			}
			return null;
		}

		public void InitLevels(string source)
		{
            Levels = JsonReader.Deserialize<LevelsModel>(source);
		}
	}
}