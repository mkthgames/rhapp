﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace App.Models
{
    sealed class LocaleStrings
    {
        public static string LOCALE_EN = "en";
        public static string LOCALE_IS = "is";

        Dictionary<string, string> _strings = new Dictionary<string, string>();

        public string this[string key]
        {
            get {
                if (_strings.ContainsKey(key)) {
                    return _strings[key];
                }
                return null;
            }

            set {
                if (!_strings.ContainsKey(key)) {
                    _strings[key] = value;
                }
            }
        }

    }

    public class StringsModel 
    {
        Dictionary<string, LocaleStrings> _locales = new Dictionary<string, LocaleStrings>();

        private static StringsModel _instance = null;
        private LocaleStrings _locale = null;
        private string _localeId = LocaleStrings.LOCALE_IS;

        public static StringsModel Instance
        {
            get
            {
                if (_instance == null) {
                    _instance = new StringsModel();
                }

                return _instance;
            }
        }

        public string CurrentLocale
        {
            get {
                return _localeId;
            }
        }
            

        public string this[string key]
        {
            get 
            {
                return Get(key);
            }
        }

        public void Init(string locale)
        {
            _locales[LocaleStrings.LOCALE_EN] = new LocaleStrings();
            _locales[LocaleStrings.LOCALE_IS] = new LocaleStrings();

            string localeData = Main.Instance.Data.ReadTextFile("locale");

            ParseLocaleData(ref localeData);

            SetLocale(locale);
        }

        public void SetLocale(string localeId)
        {
            if (_locales.ContainsKey(localeId)) 
            {
                _localeId = localeId;
                _locale = _locales[localeId];
            }
        }

        public string Get(string localeKey)
        {
            if (_locale != null)
            {
                if (_locale[localeKey] == null) {
                    Debug.LogErrorFormat("Can't find a key '{0}' in current locale", localeKey);
                } else {
                    return _locale[localeKey].Replace("\\n", "\n");
                }
            }
            return null;
        }

        void ParseLocaleData(ref string data)
        {
            string[] lines = data.Split('\n');
            foreach (string line in lines) {
                string[] tokens = line.Split(';');
                if (tokens.Length >= 3) {
                    _locales[LocaleStrings.LOCALE_EN][tokens[0]] = tokens[1];
                    _locales[LocaleStrings.LOCALE_IS][tokens[0]] = tokens[2];
                }
            }
        }
    }
}