﻿
using System.Diagnostics;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace App.Models
{
	public enum LevelsMode
	{
		None = 0,
		Base,
		Main
	}

	public class PlayerModel
	{
        public string UserLocale = string.Empty;
		public LevelsMode LevelsMode = LevelsMode.None;
		public LevelEntry SelectedLevel = null;
		public int SelectedLevelIndex = -1;
		public Stopwatch TrialTimer = new Stopwatch();
		public List<float> TrialTasksTime = new List<float>();
        public string SavedUsername = string.Empty;
        public string SavedPassword = string.Empty;

        public void SwitchLocale()
        {
            if (UserLocale == LocaleStrings.LOCALE_EN) 
            {
                UserLocale = LocaleStrings.LOCALE_IS;
            } else {
                UserLocale = LocaleStrings.LOCALE_EN;
            }
            PlayerPrefs.SetString("locale", UserLocale);
            StringsModel.Instance.SetLocale(UserLocale);
        }
	}
}