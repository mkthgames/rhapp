﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace App.Models
{
	public class LevelEntry
	{
		public string type;
        public int count;
		public int card;
        public bool sequental;
	}

	public class LevelsModel
    {
        public string tempPwd;
        public string MMPwdCustomFieldID;
        public List<int> MMid;
        public List<string> validStatus;

        //public int MMid;
		public List<LevelEntry> mainLevels;
		public List<LevelEntry> baseLevels;
	}
}