﻿using UnityEngine;
using System.Collections;

namespace App.Models
{
    public enum CardState
    {
        Back = 0,
        Flipped
    }

    public enum CardSuit
    {
        Hearts,
        Diamonds,
        Spades,
        Clubs
    }

    public class CardEntry
    {
        public CardSuit Suit = CardSuit.Hearts;
        public int Value;
    }

    public enum FlippingRule
    {
        FlipLeft = 0,
        FlipRight,
        FlipSequental
    }
}