﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace App.Models
{
    public interface IConnectionModel
    {
        string ApiKey { get; }
        string ApiSecret { get; }
        string URL { get; }
        string Locale {get; }
    }

    public class ResponseCustomField
    {
        public string id;
        public string name;
        public string value;
    }

    public class ResponseData
    {
        public string status;
        public string membership_level;
        public List<ResponseCustomField> custom_fields;

        public string this [int index] {
            get {
                for (int i = 0; i < custom_fields.Count; ++i) {
                    int fieldId = -1;
                    if (int.TryParse(custom_fields[i].id, out fieldId)) {
                        if (fieldId == index) {
                            return custom_fields[i].value;
                        }
                    }
                }
                return null;
            }
        }

        public string this[string varName]
        {
            get {
                foreach (var field in custom_fields) {
                    if (field.name == varName) {
                        return field.value;
                    }
                }
                return "";
            }
        }
    }

    public class ResponseType
    {
        public string response_code;
        public string response_message;
        public ResponseData response_data;

        public bool Success
        {
            get 
            {
                return  response_data != null;
            }
        }
    }

    public class EnglishConnectionModel: IConnectionModel
    {
        public string Locale
        {
            get
            {
                return "en";
            }
        }

        public string ApiKey
        {
            get 
            {
                return "a7hioopcxx5";
            }
        }

        public string ApiSecret
        {
            get 
            {
                return "miu4xsnq3hz";
            }
        }

        public string URL
        {
            get 
            {
                return "https://www.7minutemath.com/wp-content/plugins/membermouse/api/request.php?q=/getMember";
            }
        }
    }

    public class IslandicConnectionModel: IConnectionModel
    {
        public string Locale
        {
            get
            {
                return "is";
            }
        }

        public string ApiKey
        {
            get 
            {
                return "r1YSNeJ";
            }
        }

        public string ApiSecret
        {
            get 
            {
                return "G4o00qg";
            }
        }

        public string URL
        {
            get 
            {
                return "https://betranam.is/reiknumhradar2/wp-content/plugins/membermouse/api/request.php?q=/getMember";
            }
        }
    }
}