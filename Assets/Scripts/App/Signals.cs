﻿using UnityEngine;
using System.Collections;

using strange.extensions.signal.impl;

namespace App.Signals
{
	public class InitiateSignal: Signal {}
	public class StartLevelSignal: Signal {}

}